EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:vo14642at
LIBS:bd4848g
LIBS:fe
LIBS:ZeroBoard-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ZeroBoard - Raspberry Pi Zero Shield"
Date "2016-07-12"
Rev "0.1"
Comp "Fabio Iotti"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L USB_A P1
U 1 1 578333FE
P 1250 2750
F 0 "P1" H 1450 2550 50  0000 C CNN
F 1 "USB_A" H 1200 2950 50  0000 C CNN
F 2 "" V 1200 2650 50  0000 C CNN
F 3 "" V 1200 2650 50  0000 C CNN
	1    1250 2750
	0    -1   1    0   
$EndComp
$Comp
L CP C1
U 1 1 578481A1
P 2100 2700
F 0 "C1" H 2125 2800 50  0000 L CNN
F 1 "5V 4F" H 2125 2600 50  0000 L CNN
F 2 "" H 2138 2550 50  0000 C CNN
F 3 "" H 2100 2700 50  0000 C CNN
	1    2100 2700
	-1   0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 578481FA
P 1800 2550
F 0 "R1" V 1880 2550 50  0000 C CNN
F 1 "2.5" V 1800 2550 50  0000 C CNN
F 2 "" V 1730 2550 50  0000 C CNN
F 3 "" H 1800 2550 50  0000 C CNN
	1    1800 2550
	0    1    -1   0   
$EndComp
$Comp
L VO14642AT U1
U 1 1 57848FBE
P 3850 2650
F 0 "U1" H 3850 3100 60  0000 C CNN
F 1 "VO14642AT" H 4200 2200 60  0000 C CNN
F 2 "" H 3850 2650 60  0000 C CNN
F 3 "" H 3850 2650 60  0000 C CNN
	1    3850 2650
	1    0    0    -1  
$EndComp
$Comp
L BD4848G U2
U 1 1 578492DC
P 2850 1400
F 0 "U2" H 3000 1500 60  0000 C CNN
F 1 "BD4848G" H 3000 1000 60  0000 C CNN
F 2 "" H 2950 1450 60  0000 C CNN
F 3 "" H 2950 1450 60  0000 C CNN
	1    2850 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 2550 1650 2550
Connection ~ 2100 2850
Connection ~ 2100 2550
$Comp
L R R3
U 1 1 578495FE
P 3000 2450
F 0 "R3" V 3080 2450 50  0000 C CNN
F 1 "270" V 3000 2450 50  0000 C CNN
F 2 "" V 2930 2450 50  0000 C CNN
F 3 "" H 3000 2450 50  0000 C CNN
	1    3000 2450
	0    1    -1   0   
$EndComp
$Comp
L R R2
U 1 1 57849FB0
P 2350 2700
F 0 "R2" V 2430 2700 50  0000 C CNN
F 1 "10K" V 2350 2700 50  0000 C CNN
F 2 "" V 2280 2700 50  0000 C CNN
F 3 "" H 2350 2700 50  0000 C CNN
	1    2350 2700
	-1   0    0    1   
$EndComp
Connection ~ 2350 2850
Connection ~ 2350 2550
$Comp
L GND #PWR?
U 1 1 5784D8DC
P 1650 3000
F 0 "#PWR?" H 1650 2750 50  0001 C CNN
F 1 "GND" H 1650 2850 50  0000 C CNN
F 2 "" H 1650 3000 50  0000 C CNN
F 3 "" H 1650 3000 50  0000 C CNN
	1    1650 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 3000 1650 2850
Connection ~ 1650 2850
Wire Wire Line
	2700 2550 1950 2550
Wire Wire Line
	2600 1900 2600 2850
Wire Wire Line
	2700 1900 2700 2550
Connection ~ 2600 2850
Wire Wire Line
	2800 1900 2800 2450
Wire Wire Line
	4400 2950 4250 2950
Wire Wire Line
	4400 2100 4400 2950
Connection ~ 2700 2100
Wire Wire Line
	4250 2350 4400 2350
Connection ~ 4400 2350
Wire Wire Line
	1550 2850 3450 2850
Wire Wire Line
	2700 2100 4400 2100
Wire Wire Line
	2800 2450 2850 2450
Wire Wire Line
	3150 2450 3450 2450
$Comp
L C C2
U 1 1 5784E3BF
P 3300 2650
F 0 "C2" H 3325 2750 50  0000 L CNN
F 1 "10uF" H 3325 2550 50  0000 L CNN
F 2 "" H 3338 2500 50  0000 C CNN
F 3 "" H 3300 2650 50  0000 C CNN
	1    3300 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3300 2800 3300 2850
Connection ~ 3300 2850
Wire Wire Line
	3300 2500 3300 2450
Connection ~ 3300 2450
Wire Wire Line
	4250 2650 4700 2650
Text Notes 4750 2700 0    60   ~ 0
Stabilized VCC
$EndSCHEMATC
